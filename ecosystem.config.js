module.exports = {
  apps: [
    {
      name: 'Tarek Associates',
      exec_mode: 'cluster',
      instances: 'max',
      script: './.output/server/index.mjs'
    }
  ]
}
