var screenMdMin="1024px";

bindToggleOffcanvas();
bindToggleXsSearch();
bindHoverIntentMainNavigation();
backToHome();

function bindToggleOffcanvas() {
    $(document).on("click", ".js-toggle-sm-navigation", function () {
        toggleClassState($("main"), "offcanvas");
        toggleClassState($("html"), "offcanvas");
        toggleClassState($("body"), "offcanvas");
        resetXsSearch();
    });
}

function bindToggleXsSearch() {
    $(document).on("click", ".js-toggle-xs-search", function () {
        toggleClassState($(".site-search"), "active");
        toggleClassState($(".js-mainHeader .navigation--middle"), "search-open");
    });
}

function resetXsSearch() {
    $('.site-search').removeClass('active');
    $(".js-mainHeader .navigation--middle").removeClass("search-open");
}

function toggleClassState($e, c) {
    $e.hasClass(c) ? $e.removeClass(c) : $e.addClass(c);
    return $e.hasClass(c);
}

function bindHoverIntentMainNavigation() {

    enquire.register("screen and (min-width:" + screenMdMin + ")", {

        match: function () {
            // on screens larger or equal screenMdMin (1024px) calculate position for .sub-navigation
            $(".js-enquire-has-sub").hoverIntent(function () {
                var $this = $(this),
                    itemWidth = $this.width();
                var $subNav = $this.find('.js_sub__navigation'),
                    subNavWidth = $subNav.outerWidth();
                var $mainNav = $('.js_navigation--bottom'),
                    mainNavWidth = $mainNav.width();

                // console.log($subNav);

                // get the left position for sub-navigation to be centered under each <li>
                var leftPos = $this.position().left + itemWidth / 2 - subNavWidth / 2;
                // get the top position for sub-navigation. this is usually the height of the <li> unless there is more than one row of <li>
                var topPos = $this.position().top + $this.height();

                if (leftPos > 0 && leftPos + subNavWidth < mainNavWidth) {
                    // .sub-navigation is within bounds of the .main-navigation
                    $subNav.css({
                        "left": leftPos,
                        "top": topPos,
                        "right": "auto"
                    });
                } else if (leftPos < 0) {
                    // .suv-navigation can't be centered under the <li> because it would exceed the .main-navigation on the left side
                    $subNav.css({
                        "left": 0,
                        "top": topPos,
                        "right": "auto"
                    });
                } else if (leftPos + subNavWidth > mainNavWidth) {
                    // .suv-navigation can't be centered under the <li> because it would exceed the .main-navigation on the right side
                    $subNav.css({
                        "right": 0,
                        "top": topPos,
                        "left": "auto"
                    });
                }
                $this.addClass("show-sub");
            }, function () {
                $(this).removeClass("show-sub")
            });
        },

        unmatch: function () {
            // on screens smaller than screenMdMin (1024px) remove inline styles from .sub-navigation and remove hoverIntent
            $(".js_sub__navigation").removeAttr("style");
            $(".js-enquire-has-sub").hoverIntent(function () {
                // unbinding hover
            });
        }

    });
}

function initImager(elems) {
    elems = elems || '.js-responsive-image';
    this.imgr = new Imager(elems);
}

function reprocessImages(elems) {
    elems = elems || '.js-responsive-image';
    if (this.imgr == undefined) {
        this.initImager(elems);
    } else {
        this.imgr.checkImagesNeedReplacing($(elems));
    }
}

function addGoogleMapsApi(callback) {
    if (callback != undefined && $(".js-googleMapsApi").length == 0) {
        $('head').append('<script class="js-googleMapsApi" type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=' + ACC.config.googleApiKey + '&sensor=false&callback=' + callback + '"></script>');
    } else if (callback != undefined) {
        eval(callback + "()");
    }
}

function backToHome() {
    $(".backToHome").on("click", function () {
        var sUrl = ACC.config.contextPath;
        window.location = sUrl;
    });
}