import { defineNuxtConfig } from 'nuxt3'

// https://v3.nuxtjs.org/docs/directory-structure/nuxt.config
export default defineNuxtConfig({

  publicRuntimeConfig: {
      "baseURL": process.env.BASE_URL,
      "baseApiURL": process.env.BASE_API_URL,
      "buildAssetsDir": "/_nuxt/",
      "assetsPath": {},
      "storagePath": process.env.STORAGE_PATH,
      "cdnURL": process.env.CDN_URL
  },
    
  css: [
        '~/assets/css/bootstrap.min.css',
        '~/assets/css/header-footer.css',
        '~/assets/css/content.css',
      ],
  plugins: [
      ],

  meta: { 
    title: 'Top Education Consultant Service Agent & Firm in London, UK', meta: [],
    link: [{ rel: 'icon', type: 'image/x-icon', href: process.env.STORAGE_PATH+'Settings/favicon.png' }]
  },

  head: {
    script: [
      {
        // src: "~/assets/js/main.js",
      },
    ],
  },       

})
